package com.epam.mooc.spring.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by green on 3/18/16.
 */

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String email;
    private String name;
    private int tickets;
    private Date birthday;
    private String systemMessage;

    public User() {

    }

    public User(Long id, String email, String name, int tickets, Date birthday, String systemMessage) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.tickets = tickets;
        this.birthday = birthday;
        this.systemMessage = systemMessage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTickets() {
        return tickets;
    }

    public void setTickets(int tickets) {
        this.tickets = tickets;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSystemMessage() {
        return systemMessage;
    }

    public void setSystemMessage(String systemMessage) {
        this.systemMessage = systemMessage;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", tickets=" + tickets +
                ", birthday=" + birthday +
                ", systemMessage='" + systemMessage + '\'' +
                '}';
    }
}