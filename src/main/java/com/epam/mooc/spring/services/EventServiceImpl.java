package com.epam.mooc.spring.services;

import java.util.Date;
import java.util.List;

import com.epam.mooc.spring.entities.Event;
import com.epam.mooc.spring.repositories.AuditoriumRepository;
import com.epam.mooc.spring.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by green on 3/18/16.
 */

@Service
public class EventServiceImpl implements EventService {

    @Autowired private EventRepository eventRepository;
    @Autowired private AuditoriumRepository auditoriumRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Event> findAll() {
        return eventRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Event findByName(String name) {
        return eventRepository.findByName(name);
    }

    @Override
    @Transactional
    public void save(List<Event> events) {
        eventRepository.save(events);
    }

    @Override
    @Transactional
    public void create(Date when, String name, int price, Long auditoriumId) {
        Event event = new Event();
        event.setDate(when);
        event.setName(name);
        event.setPrice(price);
        event.setAuditorium(auditoriumRepository.findOne(auditoriumId));
        eventRepository.save(event);
    }
}