package com.epam.mooc.spring.services;

import java.util.Date;
import java.util.List;

import com.epam.mooc.spring.entities.User;

/**
 * Created by green on 3/18/16.
 */
public interface UserService {

    List<User> findAll();
    List<User> findByName(String name);
    User findById(long id);
    User findByEmail(String email);
    User save(User user);
    void save(List<User> users);
    void create(String name, String email, Date birthday);
}