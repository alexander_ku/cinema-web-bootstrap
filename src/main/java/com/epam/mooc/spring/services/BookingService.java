package com.epam.mooc.spring.services;

import java.util.Date;
import java.util.List;

import com.epam.mooc.spring.entities.Ticket;

public interface BookingService {

    void bookTickets(long userId, long eventId, Date when, List<Integer> seats);
    List<Ticket> getBookedTicketsByEvent(long eventId);
    List<Ticket> getBookedTicketsByUser(long userId);
}