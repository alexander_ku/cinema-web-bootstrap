package com.epam.mooc.spring.services;

import java.util.Date;
import java.util.List;

import com.epam.mooc.spring.entities.Event;

/**
 * Created by green on 3/18/16.
 */
public interface EventService {

    List<Event> findAll();
    Event findByName(String name);
    void save(List<Event> events);
    void create(Date when, String name, int price, Long auditoriumId);
}