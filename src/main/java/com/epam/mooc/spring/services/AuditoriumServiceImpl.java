package com.epam.mooc.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.epam.mooc.spring.entities.Auditorium;
import com.epam.mooc.spring.repositories.AuditoriumRepository;

import java.util.List;

/**
 * Created by green on 3/18/16.
 */

@Service
public class AuditoriumServiceImpl implements AuditoriumService {

    @Autowired private AuditoriumRepository auditoriumRepository;

    @Transactional(readOnly = true)
    public List<Auditorium> findAll() {
        return auditoriumRepository.findAll();
    }

    @Transactional(readOnly = true)
    @Override
    public Auditorium findByName(String name) {
        return auditoriumRepository.findByName(name);
    }
}