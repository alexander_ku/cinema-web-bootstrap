package com.epam.mooc.spring.services;

import com.epam.mooc.spring.entities.Auditorium;

import java.util.List;

/**
 * Created by green on 3/18/16.
 */
public interface AuditoriumService {

    List<Auditorium> findAll();
    Auditorium findByName(String name);
}