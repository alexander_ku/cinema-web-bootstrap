package com.epam.mooc.spring.services;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.epam.mooc.spring.entities.Auditorium;
import com.epam.mooc.spring.entities.Event;
import com.epam.mooc.spring.entities.Ticket;
import com.epam.mooc.spring.entities.User;
import com.epam.mooc.spring.repositories.EventRepository;
import com.epam.mooc.spring.repositories.TicketRepository;
import com.epam.mooc.spring.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired private TicketRepository ticketRepository;
    @Autowired private UserRepository userRepository;
    @Autowired private EventRepository eventRepository;

    @Override
    @Transactional
    public void bookTickets(long userId, long eventId, Date when, List<Integer> seats) {
        User user = userRepository.findOne(userId);
        Event event = eventRepository.findOne(eventId);
        Auditorium auditorium = event.getAuditorium();
        List<Integer> vipSeats = Arrays.stream(auditorium.getVipSeats().split(",")).map(Integer::parseInt).collect(toList());
        List<Ticket> ticketsToBook = seats.stream().map(seat -> {
            Ticket t = new Ticket();
            t.setUser(user);
            t.setEvent(event);
            t.setDate(when);
            t.setSeat(seat);
            int price = !vipSeats.contains(seat) ? event.getPrice() : event.getPrice() * 2;
            t.setPrice(price);
            return t;
            }).collect(Collectors.toList());
        ticketRepository.save(ticketsToBook);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> getBookedTicketsByEvent(long eventId) {
        return ticketRepository.findByEventId(eventId);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Ticket> getBookedTicketsByUser(long userId) {
        return ticketRepository.findByUserId(userId);
    }
}