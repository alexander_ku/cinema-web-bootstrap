package com.epam.mooc.spring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.epam.mooc.spring.entities.User;
import com.epam.mooc.spring.repositories.UserRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by green on 3/18/16.
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findByName(String name) {
        return userRepository.findByNameContaining(name);
    }
    
    @Override
    @Transactional(readOnly = true)
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    @Transactional(readOnly = true)
    public User findById(long id) {
        return userRepository.findOne(id);
    }

    @Override
    @Transactional
    public User save(User user) {
        User savedUser = userRepository.saveAndFlush(user);
        return savedUser;
    }

    @Override
    @Transactional
    public void save(List<User> users) {
        userRepository.save(users);
    }

    @Override
    @Transactional
    public void create(String name, String email, Date birthday) {
        User user = new User();
        user.setName(name);
        user.setEmail(email);
        user.setBirthday(birthday);
        userRepository.save(user);
    }
}