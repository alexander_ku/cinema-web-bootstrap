package com.epam.mooc.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.epam.mooc.spring.entities.Auditorium;

import java.util.List;

/**
 * Created by green on 3/18/16.
 */

@Repository
public interface AuditoriumRepository extends JpaRepository<Auditorium, Long> {

    List<Auditorium> findAll();
    Auditorium findByName(String name);
}