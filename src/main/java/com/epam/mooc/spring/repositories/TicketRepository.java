package com.epam.mooc.spring.repositories;

import java.util.List;

import com.epam.mooc.spring.entities.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by green on 3/18/16.
 */

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    List<Ticket> findAll();
    List<Ticket> findByEventId(long eventId);
    List<Ticket> findByUserId(long userId);
}