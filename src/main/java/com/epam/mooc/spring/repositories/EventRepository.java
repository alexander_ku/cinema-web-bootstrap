package com.epam.mooc.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.epam.mooc.spring.entities.Event;

import java.util.List;

/**
 * Created by green on 3/18/16.
 */

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findAll();
    Event findByName(String name);
}