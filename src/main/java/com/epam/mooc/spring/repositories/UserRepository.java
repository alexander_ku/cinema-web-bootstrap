package com.epam.mooc.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.epam.mooc.spring.entities.User;

import java.util.List;

/**
 * Created by green on 3/18/16.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findByNameContaining(String name);
    User findByEmail(String email);
}